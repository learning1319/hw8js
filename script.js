/* 1) DOM - модель веб документу, має структуру дерева і дозволяє взаємодіяти з елементами іншим програмам та скриптам.
2) innerHTML - повертає весь вміст елемента (текст, теги тощо), innerText - повертає лише текстовий вміст елемента
3) До елемента можна звернутися за допомогою методів пошуку, звертаючись до тега, класу, атрибуту чи id. 
  Також можна звертатися до елемента через інший елемент, як до сусіднього/батьківського/дочірнього елементу.
  Вибір способу пошуку залежить від задачі, але для мене одним з найзручніших здається спосіб пошууку за селектором.
*/
//1
const changeColorP = document.getElementsByTagName('p');
for(let elem of changeColorP){
    elem.style = "background-color:#ff0000"
}
//2
const elemById = document.getElementById("optionsList");
console.log(elemById);
const parentElemById = elemById.parentNode;
console.log(parentElemById);
const ndListElemById = elemById.childNodes;
for(let node of ndListElemById){
    console.log(`${node} +${typeof node}`)
};
//3
const elemByClass = document.querySelector("#testParagraph");

elemByClass.textContent = elemByClass.textContent.replace(`${elemByClass.innerText}`, "This is a paragraph");
//4
const childMainHeader = document.getElementsByClassName("main-header")[0].children;
console.log(childMainHeader);
for(let elem of childMainHeader){
    elem.classList.add("nav-item");

}
//6
const removeElemClass = document.getElementsByClassName("section-title");
console.log(removeElemClass);

while(removeElemClass.length > 0){
    removeElemClass[0].classList.remove('section-title'); 
   
}
